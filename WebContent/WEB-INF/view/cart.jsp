<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.*, com.ashitsathish.mvc.model.ShoppingProduct"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Shopping Cart</title>
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
  	.h1Color{
  		color: green;
  	}
  	
.table {
    border-radius: 5px;
    width: 50%;
    margin: 0px auto;
    float: none;
    }
    
    form { 
margin: 0 auto; 
width:250px;
}
  h1,h2,p{
   text-align:center;
  }

  </style>
<body>
		<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="main.jsp"></jsp:include>
<div class="container">
  <h2>Shopping Cart</h2>
  <p>The below is the list of our products you can purchase</p>   
  
  <%String message = (String) request.getAttribute("message");
	if(message != null){%>
		<h1 class="h1Color"><%= message %></h1>
	<%}%>
	
<form action="cart" method="post">      
<%
List<ShoppingProduct> items =(List<ShoppingProduct>)session.getAttribute("products");
	/* session.setAttribute("items",listofItems);
	List<ShoppingProduct> items = (List<ShoppingProduct>)session.getAttribute("items"); */
	if(items!=null){
%>
<table class="table table-striped">
    <thead>
      <tr>
      	<th>Select the product</th>
        <th>Id</th>
        <th>Name</th>
        <th>Price</th>
        <th>Quantity</th>
      </tr>
    </thead>
    <tbody>
   
       <%for(ShoppingProduct item: items){
       %>
       <tr>
       		<td><input type="checkbox" name="checkedItem" value="<%=item.getId()%>"/></td>
       		<td><%=item.getId()%></td>
       		<td><%=item.getName() %></td>
       		<td><%=item.getPrice() %></td>
       		<td><input type="text" name="<%=item.getId()%>" maxlength="4" size="4"/></td>
       </tr>		
       
       <%}%>
    </tbody>
  	</table> 
   <input type="submit" class="btn btn-primary"name="action1" value="Add to Cart"/>
   <input type="submit" class="btn btn-primary" name="action1" value="Checkout"/>
   <input type='hidden' name='page' value='cart' />
  
  <%} %>
</form>     
</div>	

<% if(items==null){ %>
	<h1>Sorry No Items</h1>
<%}%>

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>