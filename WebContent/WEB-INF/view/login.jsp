<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login Page</title>
 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style>
  	.login, .error{
  		color: red;
  	}
 
  </style>
<body>
<form:form class="form-inline" action="cart" method="post" modelAttribute="login">


 <% if(request.getAttribute("loginError") == null){%>
	<h1>Please Login</h1>
<% }%>

<% 
	if(request.getAttribute("loginError") != null){ 
	String errorMessage = (String)request.getAttribute("loginError");
%>
	<h1 class="login"><%= errorMessage %></h1>
<%}%> 
  <div class="form-group">
      <label for="username">UserName:</label>
      <form:input type="text" class="form-control" id="username" 
      placeholder="Enter username" path="username"/>
    <form:errors path="username" cssClass="error" />
  </div>
  <br>
  <div class="form-group">
      <label for="pwd">Password:</label>
      <form:input type="password" class="form-control" id="pwd" 
      placeholder="Enter password" path="password"/>
      <form:errors path="password" cssClass="error" />
  </div>
   <br>
    <input type="submit" class="btn btn-primary" name="action" value="Submit"/>
    <input type="reset" class="btn btn-danger" value="Reset"/>
</form:form>         
</body>
</html>