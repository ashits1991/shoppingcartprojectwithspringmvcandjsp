package com.ashitsathish.mvc.model;

public class CompleteShoppingProduct {
	
	private ShoppingProduct shoppingProduct;
	private int quantity;
	
	
	
	
	public CompleteShoppingProduct(ShoppingProduct shoppingProduct, int quantity) {

		this.shoppingProduct = shoppingProduct;
		this.quantity = quantity;
	}
	public ShoppingProduct getShoppingProduct() {
		return shoppingProduct;
	}
	public void setShoppingProduct(ShoppingProduct shoppingProduct) {
		this.shoppingProduct = shoppingProduct;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public String toString() {
		return "CompleteShoppingProduct [shoppingProduct=" + shoppingProduct + ", quantity=" + quantity + "]";
	}
	
	
	
}
