package com.ashitsathish.mvc.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Login {
	
	@NotNull(message="is required")
	@Size(min=3,message="minimum more than three characters required")
	private String username;
	
	@NotNull(message="is required")
	@Size(min=3,message="is required")
	private String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
