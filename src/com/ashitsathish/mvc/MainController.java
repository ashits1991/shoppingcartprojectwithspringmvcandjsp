package com.ashitsathish.mvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ashitsathish.mvc.model.CompleteShoppingProduct;
import com.ashitsathish.mvc.model.DbUtil;
import com.ashitsathish.mvc.model.Login;
import com.ashitsathish.mvc.model.LoginBean;
import com.ashitsathish.mvc.model.ShoppingProduct;

@Controller
public class MainController {
	

	
	private DbUtil dbUtil;
	private List<CompleteShoppingProduct> selectedProducts;
	
	@Autowired
	public void setDataSource( DbUtil dbUtil) {
		this.dbUtil = dbUtil;
	}

//	@RequestMapping(value="/",method=RequestMethod.POST)
	@RequestMapping("/")
	public String welcomePage(Model model) {
		model.addAttribute("login", new Login());
		return "login";
	}
	
	@RequestMapping(value="/cart" ,method=RequestMethod.POST)
	public String cartPage(@Valid @ModelAttribute("login") Login login,BindingResult bindingResult  ,HttpServletRequest request,
			Model model) {
		String username = login.getUsername();
		String password = login.getPassword();
		boolean result = loginService(username, password);
		System.out.println(result);
		if(bindingResult.hasErrors()) {
			return "login";			
		}else if(result){
			List<ShoppingProduct> products = this.dbUtil.getShoppingProducts();
//			model.addAllAttributes("cart",data);
			HttpSession session= request.getSession(true);
			session.setAttribute("products", products);
			System.out.println(request.getSession(false));
			System.out.println(request.getSession().getId());
			return "cart";
		}
		else {
			model.addAttribute("loginError","Incorrect username or password, Please try again");
			return "login";
		}
	}
		 
	



	@RequestMapping(value="/cart",method=RequestMethod.POST)
	public String cartPage1(HttpServletRequest request){	
	String action = request.getParameter("action1");
	if(action.trim().equalsIgnoreCase("Add to Cart")) {
		selectedProducts = new ArrayList();

		String[] idArr = request.getParameterValues("checkedItem");	
		for(String s: idArr){
			int id = Integer.valueOf(s);
			ShoppingProduct individualProduct = dbUtil.getIndividualProductFromDb(id);
			String quantityInString = request.getParameter(s);
			if(quantityInString != null) {
			int quantity = Integer.valueOf(quantityInString);
			CompleteShoppingProduct completeproduct = new CompleteShoppingProduct(individualProduct ,quantity);
			selectedProducts.add(completeproduct);	
			}
		}
		request.setAttribute("message", "Items added to the list");
		return "cart";
		} else if(action.trim().equalsIgnoreCase("Checkout")) {
		System.out.println(request.getSession(false));
		System.out.println(request.getSession().getId());
		HttpSession session = request.getSession(false);
		session.setAttribute("selectedProudcts",peformFinalBill(session));
		return "checkout";
		
	}
	return null;
	
	}
	
	@RequestMapping(value="/checkout",params="action" ,method=RequestMethod.POST)
	public String checkout(HttpServletRequest request) {
		String action = request.getParameter("action");
		System.out.println(action);
		if(action.trim().equalsIgnoreCase("Back to Cart")) {
			return "cart";
		} else if(action.trim().equalsIgnoreCase("Thank You")) {
			return "thankyou";
		}
		return null;
	}
	
	
	@RequestMapping(value="/process",params="actionCheck" ,method=RequestMethod.POST)
	public String processInfo(HttpServletRequest request) {
		String action = request.getParameter("actionCheck");
		if(action.trim().equalsIgnoreCase("Help")) {
			return "help";
		} else if(action.trim().equalsIgnoreCase("Log Out")) {
			System.out.println(request.getSession(false));
			System.out.println(request.getSession().getId());
			HttpSession session = request.getSession(false);
			session.invalidate();
			return "login";
		}
		return null;
	}
	
	
	private boolean loginService(String username, String password) {
		List<Login> logins = dbUtil.getLoginInfo();
		for(Login login: logins) {
			if(username.equals(login.getUsername()) && 
					password.equals(login.getPassword())) {
				return true;
			}
		}
		return false;
		
	}
	
	private HashMap<String, Double> peformFinalBill(HttpSession session) {
		double finalPrice =0;
		HashMap<String, Double> hashMap = new HashMap();
		if(selectedProducts!=null) {
		for(CompleteShoppingProduct s :selectedProducts) {
		String name  = s.getShoppingProduct().getName();
		double productsPrice = s.getQuantity()*s.getShoppingProduct().getPrice();
		finalPrice += productsPrice; 
		hashMap.put(name, productsPrice);
		}
		}
		session.setAttribute("finalPrice", finalPrice);
		return hashMap;
		}
}
	

